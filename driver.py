import os
import zipfile
from selenium import webdriver

from settings import background_js, manifest_json


class Driver:
    @staticmethod
    def get_chromedriver(use_proxy=False, user_agent=None, proxy=None, chrome_driver=''):
        host = proxy['host']
        port = proxy['port']
        user = proxy['user']
        password = proxy['pass']
        chrome_options = webdriver.ChromeOptions()
        background_js_format = background_js % (host, port, user, password)
        if use_proxy:
            plugin_file_name = f'proxy_auth_plugin{host}.zip'
            plugin_file = os.path.join('plugin_folder', plugin_file_name)

            with zipfile.ZipFile(plugin_file, 'w') as zp:
                zp.writestr("manifest.json", manifest_json)
                zp.writestr("background.js", background_js_format)
            chrome_options.add_extension(plugin_file)
        if user_agent:
            chrome_options.add_argument('--user-agent=%s' % user_agent)
        return webdriver.Chrome(chrome_driver, chrome_options=chrome_options)

    @staticmethod
    def get_chromedriver_proxy_without_login(proxy_url: str, chrome_driver='', user_agent=None):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--proxy-server=%s' % proxy_url)
        if user_agent:
            chrome_options.add_argument('--user-agent=%s' % user_agent)
        return webdriver.Chrome(chrome_driver, chrome_options=chrome_options)

    @staticmethod
    def get_chromedriver_not_use_proxy(chrome_driver='', user_agent=None):
        chrome_options = webdriver.ChromeOptions()
        if user_agent:
            chrome_options.add_argument('--user-agent=%s' % user_agent)
        return webdriver.Chrome(chrome_driver, chrome_options=chrome_options)
