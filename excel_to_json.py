import os

import pandas
import json
import codecs

BROWSER_CONFIG = {
    'chrome': '../chromedriver',
    'firefox': '../geckodriver',
    'edge': '../msedgedriver'
}


def create_profile():
    excel = pandas.read_excel('account.xlsx')
    json_str = excel.to_json(orient='records')
    list_account = json.loads(json_str)
    for i in range(len(list_account)):
        account = list_account[i]
        file = codecs.open(f'profile/{account["browser"]}{i}.json', 'w', 'utf-8')
        if 'proxy_user' in account:
            account['proxy_user'] = str(account['proxy_user'] or '')
        if 'proxy_pass' in account:
            account['proxy_pass'] = str(account['proxy_pass'] or '')
        if 'start_url' in account:
            account['start_url'] = str(account['start_url'] or '')
        if 'proxy_url' in account:
            account['proxy_url'] = str(account['proxy_url'] or '')
        if 'username' in account:
            account['username'] = str(account['username'] or '')
        if 'password' in account:
            account['password'] = str(account['password'] or '')
        if 'cookies' in account:
            cookies = account['cookies'] or '[]'
            account['cookies'] = json.loads(cookies)
        account['driver_path'] = BROWSER_CONFIG[account.get('browser', 'chrome')]

        file.write(json.dumps(account, ensure_ascii=False))


def delete_profile():
    for file in os.listdir('profile'):
        if file.endswith('.json'):
            file_dir = os.path.join('profile', file)
            os.remove(file_dir)
