import random
import time
from typing import Union

from selenium import webdriver

from driver import Driver


def get_chrome_driver(data: dict):
    if not data.get('proxy_url'):
        return Driver.get_chromedriver_not_use_proxy(chrome_driver=data['driver_path'],
                                                     user_agent=data.get('user_agent'))

    proxy_user = data.get('proxy_user')
    proxy_pass = data.get('proxy_pass')
    user_agent = data.get('user_agent')
    if not proxy_user or not proxy_pass:
        return Driver.get_chromedriver_proxy_without_login(proxy_url=data['proxy_url'], user_agent=user_agent,
                                                           chrome_driver=data['driver_path'])

    url = data['proxy_url']
    host, port = url.split(':')
    proxy_ = {
        'host': host,
        'port': port,
        'user': data['proxy_user'],
        'pass': data['proxy_pass'],
    }
    return Driver.get_chromedriver(use_proxy=True, proxy=proxy_, user_agent=user_agent,
                                   chrome_driver=data['driver_path'])


def get_firefox_driver(data: dict):
    if data.get('user_agent'):
        profile = webdriver.FirefoxProfile()
        profile.set_preference("general.useragent.override", data['user_agent'])
        firefox = webdriver.Firefox(executable_path=data['driver_path'], firefox_profile=profile)
        firefox.execute_script('return navigator.userAgent')
        return firefox
    else:
        return webdriver.Firefox(executable_path=data['driver_path'])


def get_edge_driver(data: dict):
    return webdriver.Edge(executable_path=data['driver_path'])


def get_driver(data: dict, broser_name: str):
    if broser_name == 'chrome':
        return get_chrome_driver(data)
    elif broser_name == 'firefox':
        return get_firefox_driver(data)
    elif broser_name == 'edge':
        return get_edge_driver(data)
    else:
        return get_chrome_driver(data)


def get_chrome_url(chrome, data: dict):
    cookies = data.get('cookies')
    url = data['start_url']
    print(data.get('proxy_url'), f'go to {url}')
    if cookies:
        print(data.get('proxy_url'), 'login with cookies')
        get_url_with_cookies(chrome, cookies, url)
    else:
        print(data.get('proxy_url'), 'login with username and password')
        get_url_without_cookies(chrome, url, data['username'], data['password'])


def get_url_with_cookies(driver, cookies, url):
    driver.delete_all_cookies()
    driver.get(url)
    for cookie in cookies:
        try:
            driver.add_cookie(cookie)
        except:
            pass
    driver.get(url)
    time.sleep(1)
    driver.maximize_window()


def get_url_without_cookies(driver, url, user, password):
    driver.get(url)
    while True:
        try:
            button = driver.find_element_by_xpath("//button[contains(text(), 'Log in')]")
            button.click()
            break
        except:
            pass
    time.sleep(3)
    get_button_wait_to_element(driver, '//*[@id="login-username"]').send_keys(user)
    time.sleep(1)
    get_button_wait_to_element(driver, '//*[@id="login-password"]').send_keys(password)
    get_button_wait_to_element(driver, '//*[@id="login-button"]').click()
    time.sleep(1)
    driver.get(url)
    driver.maximize_window()


def get_button_wait_to_element(driver: Union[webdriver.Chrome, webdriver.Firefox], pattern: str, count: int = 60,
                               pos: int = None):
    times = 0
    while True:
        times += 1
        if times > count:
            return None
        button = None
        try:
            if pos is None:
                button = driver.find_element_by_xpath(pattern)
            else:
                button = driver.find_elements_by_xpath(pattern)[pos]
        except:
            pass
        if button:
            time.sleep(1)
            return button
        time.sleep(1)


def get_buttons_wait_to_element(driver: Union[webdriver.Chrome, webdriver.Firefox], pattern: str, count: int = 60):
    times = 0
    while True:
        times += 1
        if times > count:
            return []
        buttons = None
        try:
            buttons = driver.find_elements_by_xpath(pattern)
        except:
            pass
        if buttons:
            time.sleep(1)
            return buttons
        time.sleep(1)


def get_random_no_conflict(prev, number):
    if number == 1:
        return 0

    while True:
        result = random.randint(0, number - 1)
        if prev != result:
            return result


def get_count_song(driver: webdriver.Chrome):
    list_music: list = get_buttons_wait_to_element(driver, '//li[@class="tracklist-row tracklist-row--oneline"]')
    list_music.extend(
        driver.find_elements_by_xpath('//li[@class="tracklist-row tracklist-row--active tracklist-row--oneline"]'))
    return len(list_music)


if __name__ == '__main__':
    print(get_random_no_conflict(-1, 5))
    print(get_random_no_conflict(2, 5))
