import codecs
import json
import os
import time
from threading import Thread

from helpers import get_driver, get_chrome_url
from spotify import listen
from excel_to_json import create_profile, delete_profile


def run_driver(data: dict, browser: str):
    while True:
        driver = get_driver(data, browser)
        try:
            get_chrome_url(driver, data)
            time.sleep(3)
            listen(driver, data)
        except Exception as e:
            print(data.get('proxy_url'), 'run fail so quit ', str(e))
        finally:
            driver.quit()


def run():
    delete_profile()
    create_profile()
    threads = []

    for file in os.listdir('profile'):
        if file.endswith('.json'):
            file_dir = os.path.join('profile', file)
            browser_data = json.load(codecs.open(file_dir, encoding='utf-8'))
            if file.startswith('chrome'):
                chrome = Thread(target=run_driver, args=(browser_data, 'chrome'))
                threads.append(chrome)
            elif file.startswith('firefox'):
                firefox = Thread(target=run_driver, args=(browser_data, 'firefox'))
                threads.append(firefox)
            elif file.startswith('edge'):
                edge = Thread(target=run_driver, args=(browser_data, 'edge'))
                threads.append(edge)

    for t in threads:
        t.start()
    for t in threads:
        t.join()


if __name__ == '__main__':
    run()
