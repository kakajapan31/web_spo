selenium==3.141.0
python-dotenv==0.7.1
youtube_dl==2020.1.24
pyinstaller
pandas==1.0.1
xlrd==1.2.0