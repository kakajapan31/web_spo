import random
import time

from helpers import get_random_no_conflict, get_count_song, get_button_wait_to_element, get_buttons_wait_to_element


def listen(driver, data: dict):
    count_song = get_count_song(driver)
    print(data.get('proxy_url'), f'there are {count_song} musics')
    if count_song == 0:
        return

    i = -1
    while True:
        follow_singer(driver, data)
        i = get_random_no_conflict(i, count_song)
        time_view = random.randint(data['min_time_view'], data['max_time_view'])
        print(data.get('proxy_url'), f'listen music {i} in {time_view} seconds')
        if count_song == 1:
            driver.get(data['start_url'])
            time.sleep(3)
        driver.execute_script(
            f'var tars = document.getElementsByClassName("tracklist-name ellipsis-one-line");'
            f'var clickEvent = document.createEvent("MouseEvents");'
            f'clickEvent.initEvent("dblclick", true, true);'
            f'tars[{i}].dispatchEvent(clickEvent)')
        time.sleep(time_view)
        like_song(driver, data)


def like_song(driver, data: dict):
    like_rate = data.get('like_rate') or 0
    if random.randint(0, 99) >= like_rate:
        return
    buttons = get_buttons_wait_to_element(driver, '//button[@class="control-button spoticon-heart-16"]')
    if len(buttons):
        button = buttons[0]
        button_title = button.get_attribute('title')
        if 'save to your liked songs' in button_title.lower():
            print(data.get('proxy_url'), 'to like this song')
            button.click()
            time.sleep(5)


def follow_singer(driver, data: dict):
    follow_rate = data.get('follow_rate')
    if random.randint(0, 99) >= follow_rate:
        return
    button = get_button_wait_to_element(driver, '//button[contains(., "Follow")]')
    if button:
        if button.text.lower() == 'follow':
            print(data.get('proxy_url'), 'to follow this singer')
            button.click()
            time.sleep(5)
